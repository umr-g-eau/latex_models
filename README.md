# Des modèles Latex pour les rapports institutionnels

## Installation et contenu du package
Pour récupérer ces modèles, deux solutions :

* Cloner le dépôt git avec la ligne de commande : `git clone https://prenom.nom@gitlab.irstea.fr/david.dorchies/latex_models.git` en remplaçant `prenom.nom` par votre identifiant gitlab.
* Télécharger et dézipper l'archive : https://gitlab.irstea.fr/david.dorchies/latex_models/repository/archive.zip?ref=master

Liste des modèles disponibles :

* Modèle de rapport INRAE
* Modèle de présentation INRAE


## Mode d'emploi

Le document maître du rapport se situe à la racine (`rapport_inrae.tex`...), les documents dépendant du modèle sont dans dossier portant le même nom que le document maître.

Le contenu du rapport (qui peut être indépendant du modèle utilisé) se trouve dans le dossier `contenu`.


## Signaler un bug / proposer une amélioration

Vous pouvez me contacter par mail david.dorchies@inrae.fr ou poster un ticket https://gitlab.irstea.fr/david.dorchies/latex_models/issues/new.

Vous pouvez aussi forker ce projet et proposer des pull-request. Concernant la charte de programmation, plusieurs principes guident le code de ce modèle :

* Eviter autant que possible la redondance de code (d'où l'utilisation du dossier `contenu` commun et la référence à `rapport_francais` pour tous les rapports en français)
* Documenter l'utilisation des packages et macros avec un lien donnant le contexte d'utilisation du package ou de la macro.